import React from "react";
import JobBrief from "./jobBrief";

 const JobBriefList = () =>{
var data = JSON.parse(localStorage.getItem("temp"));
 var city = JSON.parse(localStorage.getItem("city"));
alert(JSON.stringify(data))

     return (
         <div>

    {data && data.includes("Full-stack developer") && <JobBrief jobtitle="Full-stack developer" location="Delhi" salary="50K-60K" txt="Experience and understanding of MERN stack (Mongo DB, Express.js, React Js, Node.js) or similar frameworks like AngularJS, Vue Js, D3 Js, etc. will be appreciated"/>}

 {data && data.includes("Front-end developer") &&  <JobBrief jobtitle="Front-end developer" location="Pune" salary="35K-40K" txt="We are looking for an excellent experienced person in Backend Developer field. Be a part of a vibrant, rapidly growing tech enterprise with a great working environment. As an Backend developer you will be responsible for the server side of our web applications and you will work closely with our engineers to ensure the system consistency and improve your experience"/> }
 {data && data.includes("Back-end developer") &&   <JobBrief jobtitle="Back-end developer" location="Mumbai" salary="50K-60K" txt="We are looking for a talented User Experience Designer to create amazing user experiences. The ideal candidate should have an eye for clean and artful design, possess superior UI skills and be able to translate high-level requirements into interaction flows and artifacts, and transform them into beautiful, intuitive, and functional user interfaces. Expert understanding of contemporary user-centered design methodologies for web & mobile is a must"/> }



    {city && city.includes("Delhi") && <JobBrief jobtitle="Full-stack developer" location="Delhi" salary="50K-60K" txt="Experience and understanding of MERN stack (Mongo DB, Express.js, React Js, Node.js) or similar frameworks like AngularJS, Vue Js, D3 Js, etc. will be appreciated"/>}

 { city && city.includes("Pune") && <JobBrief jobtitle="Front-end developer" location="Pune" salary="35K-40K" txt="We are looking for an excellent experienced person in Backend Developer field. Be a part of a vibrant, rapidly growing tech enterprise with a great working environment. As an Backend developer you will be responsible for the server side of our web applications and you will work closely with our engineers to ensure the system consistency and improve your experience"/> }
 {city && city.includes("Mumbai") && <JobBrief jobtitle="Back-end developer" location="Mumbai" salary="50K-60K" txt="We are looking for a talented User Experience Designer to create amazing user experiences. The ideal candidate should have an eye for clean and artful design, possess superior UI skills and be able to translate high-level requirements into interaction flows and artifacts, and transform them into beautiful, intuitive, and functional user interfaces. Expert understanding of contemporary user-centered design methodologies for web & mobile is a must"/> }

  {data == null && <JobBrief jobtitle="Full-stack developer" location="Delhi" salary="50K-60K" txt="Experience and understanding of MERN stack (Mongo DB, Express.js, React Js, Node.js) or similar frameworks like AngularJS, Vue Js, D3 Js, etc. will be appreciated"/>}

 {data == null  && <JobBrief jobtitle="Front-end developer" location="Pune" salary="35K-40K" txt="We are looking for an excellent experienced person in Backend Developer field. Be a part of a vibrant, rapidly growing tech enterprise with a great working environment. As an Backend developer you will be responsible for the server side of our web applications and you will work closely with our engineers to ensure the system consistency and improve your experience"/> }
 {data == null  && <JobBrief jobtitle="Back-end developer" location="Mumbai" salary="50K-60K" txt="We are looking for a talented User Experience Designer to create amazing user experiences. The ideal candidate should have an eye for clean and artful design, possess superior UI skills and be able to translate high-level requirements into interaction flows and artifacts, and transform them into beautiful, intuitive, and functional user interfaces. Expert understanding of contemporary user-centered design methodologies for web & mobile is a must"/> }

         </div>

     );
 }
 export default JobBriefList;