import React from 'react';
import ReactDom from 'react-dom';
import JobBriefList from './jobBriefList';
import Demo from './demo';
import Profile from './Profile';

import {BrowserRouter,Route,Switch,useHistory} from 'react-router-dom';
const Home=()=>{
	return(
		   <div className="body" style={{marginLeft:'160px'}}>
        <Demo />
       <JobBriefList />
       
      <div> <button  style={{marginTop:'10px'}}>Prev</button> <button>Next</button></div>
   </div>
		)
}
const App = () =>{
    return (
    	<>
<Route path='/' exact component={Home}/>
<Route path='/profile' exact component={Profile}/>

   </>
    );
    
}
ReactDom.render(<BrowserRouter><App/></BrowserRouter>,document.querySelector('#root'));
