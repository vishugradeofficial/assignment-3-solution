import React,{useState,useEffect} from "react";
 const Education = props =>{
 	const [state,setState]=useState({})
    const [tempstate,settempState]=useState({})
 	var obj={}
   useEffect(()=>{
  if(localStorage.getItem("education") != null)
  {

  setState(JSON.parse(localStorage.getItem("education")))
    settempState(JSON.parse(localStorage.getItem("education")))
  }

 },[])
 	const handleChange=(e)=>{
       		obj[e.target.name] =e.target.value
 		if(Object.keys(state).length <1)
 		setState(obj)
 	else
 		setState({...state,...obj})
 		console.log(JSON.stringify(state))
 	}

        const handleSubmit=(e)=>{
        e.preventDefault()

    if(Object.keys(state).length > 1)
    {
        if(localStorage.getItem('education') != null)
      {
        var temp=[]
        temp=JSON.parse(localStorage.getItem('education'))
        temp.push(state)
        localStorage.setItem("education",JSON.stringify(temp))
        setState({})
      }
      else
      {
        var temp=[]
        temp.push(state)
        localStorage.setItem("education",JSON.stringify(temp))
        setState({})
        window.location.reload()

      }
      alert("data saved successfully")
         window.location.reload()
      
    }

  }
    const deleteEdu=(e)=>{
    var delid=e.target.getAttribute('delid')
    console.log("____________________________________")
    Object.keys(tempstate).map(elem=>{
      // alert(JSON.stringify(tempstate[elem]))
    if(tempstate[elem] != null && tempstate[elem]['degree'] == delid)
    {
      // alert("delete")
      tempstate[elem]=undefined
       localStorage.setItem("education",JSON.stringify(tempstate))
       settempState(tempstate)
       window.location.reload()

    }
    })

  }
    
     return (
         <div className="container px-5 mt-4">
      <form onSubmit={handleSubmit}>
  <div className="form-group">
    <label for="exampleInputEmail1">degree</label>
    <input type="text" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Jobtitle" onInput={handleChange} name="degree"/>
    
  </div>
  <div className="form-group">
    <label for="exampleInputPassword1">college</label>
    <input type="text" className="form-control" id="exampleInputPassword1" placeholder="company" onInput={handleChange} name="college"/>
  </div>
    <div className="form-group">
    <label for="exampleInputPassword1">Location</label>
    <input type="text" className="form-control" id="exampleInputPassword1" placeholder="Location" onInput={handleChange} name="location"/>
  </div>
               <label for="exampleInputPassword1">time Period</label>
  <div class="row">
    <div class="col">
      <select class="form-control form-control-sm" onInput={handleChange} name="frommon">
      	   <label for="exampleInputPassword1">from</label>
               <option></option>
  <option>jan</option>
  <option>feb</option>
  <option>mar</option>
  <option>apr</option>

</select>
    </div>
    <div class="col">
     <select class="form-control form-control-sm" onInput={handleChange} name="fromyr">
          <option></option>
  <option>2018</option>
  <option>2019</option>
  <option>2020</option>
  <option>2021</option>

</select>
    </div>
      	   <label for="exampleInputPassword1">to</label>
       <div class="col">

     <select class="form-control form-control-sm" onInput={handleChange} name="tomon">
          <option></option>
   <option>jan</option>
  <option>feb</option>
  <option>mar</option>
  <option>apr</option>
</select>
    </div>
       <div class="col">
     <select class="form-control form-control-sm" onInput={handleChange} name="toyr">
          <option></option>
  <option>2018</option>
  <option>2019</option>
  <option>2020</option>
  <option>2021</option>
</select>
    </div>
  </div>
  <button type="submit" className="btn btn-primary mt-3">save</button>
</form>
       <div >
{Object.keys(tempstate).map(elem=>{
  if(tempstate[elem] != null)
  {
  return(
<div className="my-4" style={{display:"flex",justifyContent:"space-between",flexDirection:"row",alignItems:"center"}}>
<span>
  <b>{tempstate[elem].degree}</b><br/>
  <b>{tempstate[elem].college}</b><br/>
  <b>{tempstate[elem].location}</b><br/>
  <b>{tempstate[elem].frommon} {tempstate[elem].fromyr} &nbsp;&nbsp;&nbsp;&nbsp; {tempstate[elem].tomon} {tempstate[elem].toyr}</b><br/>
</span>
<button className="btn btn-outline-danger" delid={tempstate[elem].degree} onClick={deleteEdu}>Delete</button>
</div>
  ) 
  }
  return
  })}
  </div>      
         </div>

     );
 }
export default Education;