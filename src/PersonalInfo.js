import React,{useState,useEffect} from "react";
 const PersonalInfo = props =>{
 	const [state,setState]=useState({});
 var obj={}
 useEffect(()=>{
 	if(localStorage.getItem("personalinfo") != null)
 	setState(JSON.parse(localStorage.getItem("personalinfo")))

 },[])

 	const handleChange=(e)=>{
 		obj[e.target.name] =e.target.value
 		if(Object.keys(state).length <1)
 		setState(obj)
 	else
 		setState({...state,...obj})
 		console.log(JSON.stringify(state))

 	}
     	const handleSubmit=(e)=>{
     		e.preventDefault()

 		if(Object.keys(state).length > 1)
 		{

 			alert("data saved successfully")
 		 	localStorage.setItem("personalinfo",JSON.stringify(state))
 		 	 window.location.reload()
 		}

 	}
     return (
      
         <div className="container px-5">
         <form onSubmit={handleSubmit}>
  <div className="form-row">
    <div className="form-group col-md-6">
      <label for="inputEmail4">first name</label>
      <input type="text" className="form-control" id="inputEmail4" placeholder="fbname" onInput={handleChange} value={state['fname']} name="fname"/>
    </div>
    <div className="form-group col-md-6">
      <label for="inputPassword4">last name</label>
      <input type="text" className="form-control" id="inputPassword4" placeholder="lname" autoComplete="off" onInput={handleChange} value={state['lname']} name="lname"  />
    </div>
  </div>
  <div className="form-row">
    <div className="form-group col-md-6">
      <label for="inputEmail4">Email</label>
      <input type="email" className="form-control" id="inputEmail4" placeholder="Email" onInput={handleChange} value={state['email']} name="email"/>
    </div>
    <div className="form-group col-md-6">
      <label for="inputPassword4">mobile</label>
      <input type="text" className="form-control" id="inputPassword4" placeholder="mobile" onInput={handleChange} value={state['mobile']} name="mobile"/>
    </div>
  </div>
  <div className="form-row">
    <div className="form-group col-md-6">
      <label for="inputEmail4">city</label>
      <input type="text" className="form-control" id="inputEmail4" placeholder="city" onInput={handleChange} value={state['city']} name="city"/>
    </div>
    <div className="form-group col-md-6">
      <label for="inputPassword4">postal</label>
      <input type="text" className="form-control" id="inputPassword4" placeholder="postal" onInput={handleChange} value={state['postal']} name="postal"/>
    </div>
  </div>
  <div className="form-group">
  	      <label for="inputPassword4">position and accomplish as full stack dev </label>
   <textarea className="form-control" onChange={handleChange} name="textarea" value={state['textarea']}></textarea>
  </div>
  <button type="submit" className="btn btn-primary">save</button>
</form>
             
         </div>

     );
 }
export default PersonalInfo;