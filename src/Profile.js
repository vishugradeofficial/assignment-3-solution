import React from "react";
import WorkExperience from './WorkExperience';
import PersonalInfo from './PersonalInfo';
import Education from './Education';



 const Profile = props =>{
    
     return (
         <div className="container">
         <PersonalInfo/>
         <hr/>
         <hr/>
         <WorkExperience/>
         <hr/>
         <hr/>
         <Education/>
             
         </div>

     );
 }
export default Profile;