import React from "react";
 const JobBrief = props =>{
    
     return (
         <div className="brief" style={{width:'340px'}}  >
             <div className="jobtitle">
             <h5>{props.jobtitle }</h5>
             <h6>{props.location}</h6>
             </div>
             <div className="image" >
                 <img style={{width:'200px',height:'100px'}} src="image.png"></img>
             </div>
             <div className="details">
             <p  style={{width:'300px'}}>{props.txt}</p>
             </div>
             <div className="salary">
                 Salary : {props.salary} 
             </div>
             <div className="buttons">
             <button style={{backgroundColor:'blue',color:'white'}}>Apply</button><span><button style={{backgroundColor:'red',color:'white'}}>Not Intersted</button></span>
             </div>
             <hr />
         </div>

     );
 }
export default JobBrief;